import os
import numpy as np
import pandas as pd
import xlwings as xw
import re
import ast
from utils import logger

def create_raw_source_file(transform_template):
    path = os.path.dirname(transform_template)
    arr = pd.read_excel(os.path.join(path, "Source.xlsx"), header=None, dtype=str).to_numpy()
    clean_arr = []
    for row in arr:
        non_nan_value_count = 0
        for index in range(len(row)):
            if str(row[index]) != 'nan':
                non_nan_value_count += 1
        logger('create_raw_source_file', f'non_nan_value_count: {non_nan_value_count}')
        if non_nan_value_count > 10:
            clean_arr.append(row)
    np_array = np.array(clean_arr)
    logger('create_raw_source_file', f'removing duplicate headers from {str(np_array)}')
    for index, col in enumerate(np_array.T):
        first_header = str(col[0])
        for idx, item in enumerate(col[1:]):
            if str(item) == str(first_header):
                logger('create_raw_source_file', f'duplicate header found')
                logger('create_raw_source_file', f'item: {item}, first_header: {first_header}')
                col[idx + 1] = 0
                np_array[:, index] = col
    logger('create_raw_source_file', 'removing rows that are all 0')
    np_array = np_array[~np.all(np_array == 0, axis=1)]  # remove rows that are all 0
    final_df = pd.DataFrame(data=np_array)
    final_df.rename(columns=final_df.iloc[0], inplace=True)  # set first row as columns headers
    final_df.drop(final_df.index[0], inplace=True)  # removed first row
    final_df.fillna('', inplace=True)
    final_df.replace('NONE', '', inplace=True)
    logger('create_raw_source_file', 'final_df data cleansing done')
    writer = pd.ExcelWriter(f'{path}/Source-Raw.xlsx', engine='xlsxwriter')
    logger('create_raw_source_file', f'saving file to {path}/Source-Raw.xlsx')
    final_df.to_excel(writer, sheet_name='Source-Raw', index=False, header=True)
    writer.save()
    writer.close()


def export_master_files(transform_template, inbound_path, outbound_path):
    source_sheet_name = get_source_sheet_name(inbound_path)
    df = pd.read_excel(transform_template, sheet_name=source_sheet_name, index_col=False, dtype=str)
    raw_source_df = pd.read_excel(os.path.join(inbound_path, "Source-Raw.xlsx"), sheet_name='Source-Raw',
                                  index_col=False, dtype=str)
    raw_source_df.reset_index()
    df.dropna(axis=1, how='all', inplace=True)
    function_df = df[df[1] == 'function?'].iloc[:, 1:]
    logger('export_master_files', f'get the function row: {function_df}')
    for i in range(len(function_df.columns)):
        fn = function_df.iloc[0, i]
        if str(fn) != 'nan':
            data = {}
            custom_col = []
            params = re.search( "\((.*)\)" ,fn).group(1) #get everything enclose in the outermost bracket
            split_arr = re.split(r',\s*(?![^\[\]]*\])', params)  # split by comma except comma in square bracket
            file_name = split_arr[0]
            split_arr.pop(0)
            prefix_key = None
            number_padding = None
            index_column = None
            for idx, item in enumerate(split_arr):
                if re.match("\[.*?\]", item):
                    col_arr = ast.literal_eval(item)
                    if col_arr[1].isdigit():
                        index = int(col_arr[1]) - 2
                        value = raw_source_df.iloc[:, index].to_numpy()
                        # new_data = value[~pd.isnull(value)]
                        data[col_arr[0]] = value
                    else:
                        index_column = col_arr[0]
                        data[col_arr[0]] = ""
                        code_params = list(filter(None, col_arr[1].split('#')))
                        if not code_params:
                            continue
                        else:
                            prefix_key = code_params[0] if len(code_params) > 0 else None
                            number_padding = code_params[1] if len(code_params) > 1 else None
                else:
                    custom_col.append(item)
            print(data)
            final_df = pd.DataFrame(data=data)
            logger('export_master_files', str(final_df))
            final_df.drop_duplicates(inplace=True)
            final_df.dropna(how='any', inplace=True)
            final_df.fillna("", inplace=True)
            final_df.reset_index(drop=True, inplace=True)
            if index_column:
                final_df[index_column] = final_df.index
                final_df[index_column] = final_df[index_column].astype(str).str.zfill(
                    int(number_padding)) if number_padding else final_df[index_column]
            if prefix_key:
                final_df[index_column] = prefix_key + final_df[index_column].map(str)
            for col in custom_col:
                final_df[col.strip('\"')] = np.nan
            if (final_df.empty) or (pd.isnull(final_df.iloc[0, 0])):
                continue
            writer = pd.ExcelWriter(os.path.join(outbound_path, f'{file_name}.xlsx'), engine='xlsxwriter')
            final_df.to_excel(writer, sheet_name=file_name, index=False)
            writer.save()

def get_source_sheet_name(inbound_path):
    for f in os.listdir(inbound_path):
        if 'transform' in f.lower():
            transform_template = os.path.join(inbound_path, f)
    with xw.App(visible=False) as app:
        transformWb = xw.Book(transform_template,update_links=True)
        transformSheet = transformWb.sheets['main']
        source_sheet_name = transformSheet.range('C6').value
        transformWb.save()
        transformWb.close()
        return source_sheet_name

# if __name__ == "__main__":
#     ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
#     INBOUND_PATH = f'{ROOT_DIR}/inbound'
#
#     data = {}
#     custom_col = []
#     #raw_source_df = pd.read_excel(os.path.join(INBOUND_PATH, "Source-Raw.xlsx"), sheet_name='Source-Raw',
#     #                         index_col=False)
#     function_param_regex = re.compile("\((.*)\)")  # capture everything inside ()
#     param_res = f'export_master(OFR03Code, ["TimeSoftDIVISIONCODE", "#MYJC#4"], ["OFR03Name", "50"], "CountryCodes", "CompanyCodes")'
#     params = re.search( "\((.*)\)" ,param_res).group(1)
#     split_arr = re.split(r',\s*(?![^\[\]]*\])', params)  # split by comma except comma in square bracket
#     file_name = split_arr[0]
#     split_arr.pop(0)
#     prefix_key = None
#     number_padding = None
#     index_column = None
#     for idx, item in enumerate(split_arr):
#         if re.match("\[.*?\]", item):
#             col_arr = ast.literal_eval(item)
#             if col_arr[1].isdigit():
#                 index = int(col_arr[1]) - 2
#                 value = raw_source_df.iloc[:, index].to_numpy()
#                 # new_data = value[~pd.isnull(value)]
#                 data[col_arr[0]] = value
#             else:
#                 index_column = col_arr[0]
#                 data[col_arr[0]] = ""
#                 code_params = list(filter(None, col_arr[1].split('#')))
#                 if not code_params:
#                     continue
#                 else:
#                     prefix_key = code_params[0] if len(code_params) > 0 else None
#                     number_padding = code_params[1] if len(code_params) > 1 else None
#         else:
#             custom_col.append(item)
#     print(data)
#     final_df = pd.DataFrame(data=data)
#     logger('export_master_files', str(final_df))
#     final_df.drop_duplicates(inplace=True)
#     final_df.dropna(how='any', inplace=True)
#     final_df.fillna("", inplace=True)
#     final_df.reset_index(drop=True, inplace=True)
#     if index_column:
#         final_df[index_column] = final_df.index
#         final_df[index_column] = final_df[index_column].astype(str).str.zfill(
#             int(number_padding)) if number_padding else final_df[index_column]
#     if prefix_key:
#         final_df[index_column] = prefix_key + final_df[index_column].map(str)
#     print(final_df)
