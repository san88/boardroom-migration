import glob
import os
import re
import datetime
import shutil
import subprocess
import pandas as pd
from zipfile import ZipFile
from dotenv import load_dotenv

load_dotenv()
UNZIP_KEY = os.getenv('UNZIP_KEY')
ZIP_KEY = os.getenv('ZIP_KEY')
ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
OUTBOUND_PATH = f'{ROOT_DIR}/outbound'
INBOUND_PATH = f'{ROOT_DIR}/inbound'


def reply_email(msg, reply_text):
    replymsg = msg.reply
    replymsg.HTMLbody = reply_text
    replymsg.Send()


def getcol(ws, colname):
    lastcol = ws.range('1:1').last_cell.end('left').column
    for c in range(1, lastcol + 1):
        if ws.range(1, c).value == colname:
            return c
    return 0


def create_path(path):
    if not os.path.isdir(path):
        os.makedirs(path)
        print(f'folder created: {path}')
    else:
        clear_path(path)
    return


def clear_path(path):
    if os.path.exists(path):
        files = os.listdir(path)
        if len(files) > 0:
            for f in files:
                os.remove(os.path.join(path, f))
            return


def reply_email_with_attachments_in_path(msg, zip_file_name, reply_text, outbound_path):
    ZIP_KEY = f"-p{os.getenv('ZIP_KEY')}"
    reply_msg = msg.reply
    reply_msg.HTMLbody = reply_text
    files = os.listdir(outbound_path)
    new_file_name = f"{zip_file_name.split('.zip')[0]}-Converted.zip"
    archive = os.path.abspath(os.path.join(outbound_path, f'{new_file_name}'))
    archive_files = []
    if len(files) > 0:
        for f in files:
            if os.path.isfile(os.path.abspath(os.path.join(outbound_path, f))):
                target_file = os.path.abspath(os.path.join(outbound_path, f))
                archive_files.append(target_file)
                print(f'added archive file {target_file}')
        rc = subprocess.call(["C:\\Program Files\\7-Zip\\7z.exe", 'a', ZIP_KEY, '-y', archive] +
                             archive_files)
        reply_msg.Attachments.Add(archive)
        reply_msg.Send()


def undate(x):
    if pd.isnull(x):
        return x
    if re.search(r"^\d{6}-\d{2}-\d{4}$", x):
        return x
    try:
        if '00:00:00' in x:
            return datetime.strptime(x, '%Y-%m-%d %H:%M:%S').date()
        else:
            return x
    except AttributeError:
        return x
    except Exception:
        raise


def unzip(path):
    file_name = None
    for file in os.listdir(path):
        if file.endswith(".zip"):
            file_name = file
            file_path = os.path.join(path, file)
            with ZipFile(file_path) as zf:
                items = zf.infolist()
                for item in items:
                    if item.filename[-1] == '/':
                        continue
                    item.filename = os.path.basename(item.filename)
                    zf.extract(member=item, path=INBOUND_PATH, pwd=bytes(UNZIP_KEY, 'utf-8'))
    return file_name


def unzip_7z(path):
    copy_file = lambda files, destination: [(shutil.copy(x, destination)) for x in files]
    UNZIP_KEY = f"-p{os.getenv('UNZIP_KEY')}"
    file_name = None
    for file in os.listdir(path):
        if file.endswith(".zip"):
            file_name = file
            file_path = os.path.join(path, file)
            subprocess.call(["C:\\Program Files\\7-Zip\\7z.exe", "x", UNZIP_KEY, file_path, f"-o{INBOUND_PATH}"])
    # check whether there is a folder
    file_items = os.listdir(path)
    for file in file_items:
        if os.path.isdir(os.path.join(path, file)):
            excel_files = glob.glob(os.path.join(os.path.join(path, file), '*.xlsx'))
            copy_file(excel_files, INBOUND_PATH)
            # remove the folder after copying contents
            shutil.rmtree(os.path.join(path, file))
    return file_name