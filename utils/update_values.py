from utils.logger import logger
from utils.db import db
from utils.create_db import BotRun
from utils.format_config import get_datetime
from sqlalchemy.orm import sessionmaker
from datetime import datetime

Base = db.Base
engine = db.engine

def update_botrun(botrun_id, status, remarks):
    try:
        Session = sessionmaker(bind=engine)
        session = Session()
        record = session.query(BotRun).filter(BotRun.id == botrun_id).first()
        record.status = status
        record.remarks = str(remarks)
        record.endedDT = get_datetime(datetime.now())
        record.updatedDateTime = get_datetime(datetime.now())
        session.flush()
        session.commit()
        session.close()
    except Exception as e:
        logger('update_botrun', f'{str(e)}', 'ERROR')

