import re

from utils import Emails, Bot
from utils.logger import logger
from utils.db import db
from utils.format_config import get_datetime
from sqlalchemy.orm import sessionmaker
from datetime import datetime

Base = db.Base
engine = db.engine


def select_email(email_id):
    try:
        Session = sessionmaker(bind=engine)
        session = Session()
        results = session.query(Emails).filter(Emails.entry_id == email_id).all()
        session.close()
        return results
    except Exception as e:
        logger('select_latest_email', e, 'ERROR')

def select_bot(subject):
    try:
        results = None
        Session = sessionmaker(bind=engine)
        session = Session()
        bots = session.query(Bot).all()
        for bot in bots:
            try:
                if re.search(bot.email_subject, subject):
                    results = bot
                    break
            except TypeError:
                continue
        session.close()
        return results
    except Exception as e:
        logger('select_bot', e, 'ERROR')
        return None
