from utils.logger import logger
from utils.db import db
from utils.format_config import get_datetime
from sqlalchemy.orm import sessionmaker
from datetime import datetime

Base = db.Base
engine = db.engine

def insert_botrun(botrun):
    try:
        Session = sessionmaker(bind=engine)
        logger('insert_botrun', 'Inserting value to database')
        session = Session()
        botrun.type = 2
        botrun.startedDT = get_datetime(datetime.now())
        botrun.status= 3
        botrun.remarks = ''
        botrun.createdDateTime = get_datetime(datetime.now())
        session.add(botrun)
        session.commit()
        logger('insert_botrun', 'Successfully inserted SQL data')
        session.flush()
        botrun_id = botrun.id
        session.close()
        return botrun_id
    except Exception as e:
        logger('insert_botrun', f'{str(e)}', 'ERROR')

def insert_email_log(email):
    try:
        Session = sessionmaker(bind=engine)
        logger('insert_email_log', 'Inserting value to database')
        session = Session()
        session.add(email)
        session.commit()
        logger('insert_email_log', 'Successfully inserted SQL data')
        session.flush()
        session.close()
    except Exception as e:
        logger('insert_email_log', f'{str(e)}', 'ERROR')


if __name__ == '__main__':
    print('Hello World')