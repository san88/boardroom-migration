DATE_FMT = '%Y%m%d'
TIME_FMT = '%H:%M:%S'
DATETIME_FMT = '%Y-%m-%d %H:%M:%S'
DATETIME_REQ_FMT = '%d/%m/%Y %H:%M %p'
DATETIME_FLD_FMT = '%Y%m%d_%H%M%S'

def get_date(date_obj):
    return date_obj.strftime(DATE_FMT)

def get_datetime(datetime_obj):
    return datetime_obj.strftime(DATETIME_FMT)

def get_time(datetime_obj):
    return datetime_obj.strftime(TIME_FMT)

def get_folder_datetime(datetime_obj):
    return datetime_obj.strftime(DATETIME_FLD_FMT)