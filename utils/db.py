import os
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from dotenv import load_dotenv

load_dotenv()

class App:
    def __init__(self):
        self.engine = ''
        self.Base = ''

    def connect_to_db(self):
        self.engine = create_engine(f"mssql+pyodbc://{os.getenv('SQL_DATABASE_SERVER')}/{os.getenv('SQL_DATABASE_NAME')}?driver={os.getenv('SQL_DATABASE_DRIVER')}")
        self.Base = declarative_base()


db = App()
db.connect_to_db()

if __name__ == "__main__":
    db = App()
    db.connect_to_db()
    print(db.engine)
    print(db.Base)