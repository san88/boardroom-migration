from db import sql_engine
import pandas as pd
from create_db import BotRun
from utils import logger
from sqlalchemy.orm import sessionmaker

Base, engine = sql_engine()
Session = sessionmaker(bind=engine)
session = Session()



def db_to_dataframe():
    try:
        logger('Loading DB to Pandas')
        df = pd.read_sql(str(session.query(BotRun)), session.bind)
        print(f"{df}")
        logger('Successfully loaded DB to Pandas')
        return df
    except Exception as e:
        logger(str(e), 'Error')
        return str(e)



if __name__ == '__main__':
    db_to_dataframe()

