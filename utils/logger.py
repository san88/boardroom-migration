from datetime import datetime
import os
from utils.format_config import get_date, get_datetime

def logger(method, text, log_type='INFO'):
    print(str(text))
    path = os.getcwd()
    target_path = os.path.join(path, 'Logs')
    # Checking whether log is an error
    file_path = target_path + f'/Info/'
    if log_type == 'ERROR':
        file_path = target_path + f'/Error/'
    # Create folder if it doesnt exist
    if not os.path.exists(file_path):
        os.makedirs(file_path)
    # Write to txt file
    f = open(file_path + f"{get_date(datetime.now())}.txt", "a")
    f.write(get_datetime(datetime.now()) + f' - {method} ({log_type}): ' + str(text) + '\n')
    f.close()


if __name__ == "__main__":
    logger('__main__', f"Hello World", log_type = 'Error')
