from utils.db import db
from sqlalchemy import Column, Integer, String, DateTime
from sqlalchemy.orm import sessionmaker

engine = db.engine
Base = db.Base

# JUST RUN THIS FILE ONCE

class Bot(Base):
    __tablename__ = 'bot'
    __table_args__ = {'extend_existing': True}
    id = Column(Integer, primary_key=True)
    name = Column(String(50))
    description = Column(String(100))
    email_subject = Column(String(200))
    man_hours = Column(Integer)
    task_code = Column(String(100))
    live_date = Column(DateTime)
    retire_date = Column(DateTime)


class BotRun(Base):
    __tablename__ = 'botRun'
    __table_args__ = {'extend_existing': True}
    id = Column(Integer, primary_key=True)
    bot_id = Column(Integer)
    entry_id = Column(String(200))
    email = Column(String(200))
    type = Column(Integer)
    startedDT = Column(DateTime(timezone=True))
    endedDT = Column(DateTime(timezone=True))
    status = Column(Integer)
    remarks = Column(String(7999))
    createdDateTime = Column(DateTime(timezone=True))
    updatedDateTime = Column(DateTime(timezone=True))


class Run(Base):
    __tablename__ = 'run_type'
    __table_args__ = {'extend_existing': True}
    id = Column(Integer, primary_key=True)
    name = Column(String(50))

class Status(Base):
    __tablename__ = 'status'
    __table_args__ = {'extend_existing': True}
    id = Column(Integer, primary_key=True)
    name = Column(String(50))

class Emails(Base):
    __tablename__ = 'email_logs'
    __table_args__ = {'extend_existing': True}
    id = Column(Integer, primary_key=True)
    entry_id = Column(String(200))
    date_time = Column(DateTime(timezone=True))
    task_code = Column(String(100))
    sender_name = Column(String(100))
    sender_email = Column(String(100))
    sender_location = Column(String(100))
    email_body = Column(String(7999))
    subject = Column(String(200))


def initialise_db():
    # Adding Bot types to Database
    Session = sessionmaker(bind=engine)
    session = Session()
    bot = Bot()
    bot.name = 'Naggy Bot'
    bot.description = 'The bot that autmatically helps create Jira issues and Sharepoint'
    session.add(bot)
    bot = Bot()
    bot.name = 'HRMS marking tool'
    bot.description = 'The bot that helps mark the HRMS system created by newjoiners'
    session.add(bot)
    bot = Bot()
    bot.name = 'IRAS Bot'
    bot.description = 'Iras bot performing pre/post checking'
    session.add(bot)
    bot = Bot()
    bot.name = 'Boardroom Migration Bot'
    bot.description = 'Mirgration bot that helps migrate boardroom legacy HR platform to BIPO HR platform'
    session.add(bot)
    bot = Bot()
    bot.name = 'Udesk Chatbot'
    bot.description = 'Chatbot that'
    session.add(bot)
    bot = Bot()
    bot.name = 'General Migration Bot'
    bot.description = 'General Migration bot that helps migrate General HR data to BIPO HRMS platform'
    session.add(bot)
    bot = Bot()
    bot.name = 'Pro Family Bot'
    bot.description = 'Bot that helps claim Childcare/Extended Childcare leaves for employers'
    session.add(bot)
    session.commit()

    # Adding Run types to Database
    run = Run()
    run.name = 'Scheduled Run'
    session.add(run)
    run = Run()
    run.name = 'Ad Hoc Run'
    session.add(run)
    session.commit()

    # Adding Status types to Database
    status = Status()
    status.name = 'Success'
    session.add(status)
    status = Status()
    status.name = 'Failure'
    session.add(status)
    status = Status()
    status.name = 'Pending'
    session.add(status)
    session.commit()

    # Close the session
    session.close()


if __name__ == '__main__':
    Base.metadata.create_all(bind=engine)
    # initialise_db()
