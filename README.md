Guidelines:

	1. Make sure the email subject line as below 
		a. QuickPay
			i.      #rpa #bdr-qp for [CompanyCode] pgc:[PayGroupCode] cal:[CalendarCode] lgc:[LeaveGroupCode]
			ii.     Each input must be enclosed with '[…..]' 
			iii.    Accepted CalendarCode Format (dd-mm-yyyy)
				        1)  12-05-2022 
				        2)  12/05/2022
		b. Except CalendarCode, other fields are alphanumeric property.

	2. Must attach 4 compulsory files, strictly follow the naming example as below. 
        a. QuickPay
		    i.      Source.csv 
            ii.     Nationality Master.csv 
            iii.    Bank Master.csv
            iv.     Transform template (employee).xlsx
                        1) Make sure the target sheets in the transform template are named as target(1), target(2), target(3)

For developer testing:
    
	1. Create #rpa and 'error-log' folder under your main inbox.
    2. Create #bdr-qp sub-folder under #rpa folder.
    3. Run the email listener and trigger the bot. 



    