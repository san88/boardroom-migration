# import os
# import time
# from bdr_custom_function import to_xlsx, create_raw_file, get_formatted_target, export_target_file, append_custom_column
# from export_master import create_raw_source_file,export_master_files
#
# def process_target_1(inbound_path, outbound_path, constant_path, custom_codes):
#     start_time = time.perf_counter()
#
#     print("Processing Target File 1")
#     create_raw_file(1, os.path.join(inbound_path, 'Transform Template (Employee).xlsx'), os.path.join(constant_path,'TimeSoft_Payroll_Target.xlsx'))
#     formatted_target_df = get_formatted_target(1, inbound_path,
#                                                os.path.join(inbound_path, 'Transform Template (Employee).xlsx'))
#     final_target_df = append_custom_column(custom_codes, formatted_target_df)
#     export_target_file(1, outbound_path, final_target_df)
#
#     end_time = time.perf_counter()
#     print(f"Execution Time for Target 1 : {end_time - start_time:0.6f}")
#
#
# def process_target_2(inbound_path, outbound_path, constant_path):
#     start_time = time.perf_counter()
#
#     print("Processing Target File 2")
#     create_raw_file(2, os.path.join(inbound_path, 'Transform Template (Employee).xlsx'),os.path.join(constant_path,'TimeSoft_Payroll_Target.xlsx'))
#     final_target_df = get_formatted_target(2, inbound_path,
#                                            os.path.join(inbound_path, 'Transform Template (Employee).xlsx'))
#     export_target_file(2, outbound_path, final_target_df)
#
#     end_time = time.perf_counter()
#     print(f"Execution Time for Target 2: {end_time - start_time:0.6f}")
#
#
# def process_target_3(inbound_path, outbound_path, constant_path, custom_codes):
#     start_time = time.perf_counter()
#
#     print("Processing Target File 3")
#     create_raw_file(3, os.path.join(inbound_path, 'Transform Template (Employee).xlsx'),os.path.join(constant_path,'TimeSoft_Payroll_Target.xlsx'))
#     formatted_target_df = get_formatted_target(3, inbound_path,
#                                                os.path.join(inbound_path, 'Transform Template (Employee).xlsx'))
#     final_target_df = append_custom_column(custom_codes, formatted_target_df)
#     export_target_file(3, outbound_path, final_target_df)
#
#     end_time = time.perf_counter()
#     print(f"Execution Time for Target 3: {end_time - start_time:0.6f}")
#
#
# def process_exception(inbound_path, outbound_path, constant_path, custom_codes):
#     start_time = time.perf_counter()
#
#     print("Processing Exception File")
#     create_raw_file('Exception', os.path.join(inbound_path, 'Transform Template (Employee).xlsx'),os.path.join(constant_path,'TimeSoft_Payroll_Target.xlsx'))
#     formatted_target_df = get_formatted_target('Exception', inbound_path,
#                                                os.path.join(inbound_path, 'Transform Template (Employee).xlsx'))
#     final_target_df = append_custom_column(custom_codes, formatted_target_df)
#     export_target_file('Exception', outbound_path, final_target_df)
#
#     end_time = time.perf_counter()
#     print(f"Execution Time for Target 1 : {end_time - start_time:0.6f}")
#
#
# def process_master_files(inbound_path, outbound_path):
#     start_time = time.perf_counter()
#
#     print("Exporting Master Files")
#     create_raw_source_file(os.path.join(inbound_path, 'Transform Template (Employee).xlsx'))
#     export_master_files(inbound_path, outbound_path)
#
#     end_time = time.perf_counter()
#     print(f"Execution Time for Master Files : {end_time - start_time:0.6f}")
