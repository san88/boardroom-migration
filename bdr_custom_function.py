import os
import re

import pandas as pd
import xlwings as xw
from constant import constant
from utils import logger
from rpa_control import undate
from datetime import datetime


def to_xlsx(files):
    if len(files) != 0:
        for f in files:
            file_name = os.path.splitext(os.path.basename(f))[0]
            path = os.path.dirname(f)
            with open(f, 'r') as temp_f:
                encoder = temp_f.encoding
                # get No of columns in each line
                col_count = [len(l.split(",")) for l in temp_f.readlines()]
            # Generate column names  (names will be 0, 1, 2, ..., maximum columns - 1)
            column_names = [i for i in range(0, max(col_count))]
            # Read csv
            df = pd.read_csv(f, header=None, delimiter=",", names=column_names, dtype=str, engine='python',
                             encoding=encoder)
            data_xls = df.to_excel(f'{path}/{file_name}.xlsx', sheet_name=file_name, index=False, header=None)
        return True
    else:
        return False


def append_custom_column(custom_codes, target_df):
    for key, value in custom_codes.items():
        if key in target_df.columns:
            target_df[key] = value
    return target_df


def process_target_file(key, inbound_path, outbound_path):
    app = xw.App(visible=False)
    app.display_alerts = False

    try:
        search_text = 'target'
        target_file = None
        inbound_files = [file for file in os.listdir(inbound_path) if 'xlsx' in file]
        outbound_files = [file for file in os.listdir(outbound_path) if 'xlsx' in file]

        for filename in outbound_files:
            wb = app.books.open(f'{outbound_path}/{filename}', update_links=True)

        for filename in inbound_files:
            if search_text in filename.lower():
                target_file = filename
            else:
                logger('process_target_file', f'Opening {inbound_path}/{filename}')
                wb = app.books.open(f'{inbound_path}/{filename}', update_links=True)
        logger('process_target_file', f'Opening {inbound_path}/{target_file}')
        target_wb = xw.Book(f'{inbound_path}/{target_file}', update_links=True)
        target_wb.api.RefreshAll()
        target_wb.save()
        logger('process_target_file', f'Saving target_file...')
        app.quit()
    except Exception as e:
        logger('process_target_file', e, 'ERROR')
        app.quit()


def export_target_file(key, custom_codes, inbound_path, outbound_path, custom_sheet_name):
    target_path = os.path.join(inbound_path, f'{key}_Target.xlsx')
    target_wb = pd.ExcelFile(target_path)
    sheets = target_wb.book.worksheets
    for sheet in sheets:
        if sheet.sheet_state == 'visible':
            export_sheet_name = f'{custom_sheet_name}-{sheet.title}' if custom_sheet_name != None else sheet.title
            target_df = pd.read_excel(target_path, sheet_name=str(sheet.title), dtype=str)
            for i in target_df.columns:
                target_df[i] = target_df[i].apply(undate)
            logger('export_target_file', f'target_df: {target_df}')
            if 'exception' in sheet.title.lower() and target_df.empty:  # to cater ts exception
                continue
            if 'exception' in sheet.title.lower() and pd.isnull(target_df.iloc[0, 0]):  # to cater qp exception
                continue
            target_df.drop([col for col in target_df.columns if '?' in col], axis=1, inplace=True)
            final_df = append_custom_column(custom_codes, target_df)
            final_df = final_df.loc[:, ~final_df.columns.str.contains('^Unnamed')]
            logger('export_target_file', f'appended custom columnss to final_df...')
            logger('export_target_file', f'{final_df}')
            final_df.replace({pd.NaT: None}, inplace=True)
            final_df.fillna('', inplace=True)
            final_df = final_df.applymap(str)
            final_df = final_df.astype(str)
            final_df = final_df.replace('NaT', "")
            writer = pd.ExcelWriter(f'{outbound_path}/{export_sheet_name}.xlsx', engine='xlsxwriter')
            logger('export_target_file', f'writing final_df to {outbound_path}/{export_sheet_name}.xlsx')
            final_df.to_excel(writer, sheet_name=str(export_sheet_name), index=False, header=True)
            writer.save()
            writer.close()


def rename_transform_template_sheet(inbound_path):
    for f in os.listdir(inbound_path):
        if 'transform' in f.lower():
            transform_template = os.path.join(inbound_path, f)
        if 'source' in f.lower():
            source = os.path.join(inbound_path, f)
    with xw.App(visible=False) as app:
        sourceWb = xw.Book(source)

        source_sheet_name = sourceWb.sheets[0].name
        transformWb = xw.Book(transform_template, update_links=True)

        transformSheet = transformWb.sheets['main']

        transformSource = transformSheet.range('C:C')
        null_counter = 0
        for row in transformSource:
            if row.value is None:
                null_counter += 1
            if null_counter == 20:
                break;
            if row.value == 'source':
                sourceCell = row
        # transformSheet.range('B6').value = str(sourceSheet.name)
        row = sourceCell.row
        transformSheet.range(f"B{row}").value = str(source_sheet_name)

        # save Wb
        transformWb.save()
        sourceWb.save()

        # Close Wb
        transformWb.close()
        sourceWb.close()

        return source_sheet_name


def get_sheet_title(key, title, sheet_name):
    if key == 'TimeSoft_Payroll':
        title = f'{sheet_name}-{title}'
    return title


def check_columns(inbound_path):
    target_path = os.path.join(inbound_path, 'Source.xlsx')
    # target_df = pd.read_excel(target_path, sheet_name='Report', dtype=str, header=5)
    target_df = pd.read_excel(target_path, sheet_name='Report', dtype=str, converters={'TOTAL ELIGIBLE CHILD POINT': int}, header=5)
    writer = pd.ExcelWriter(f'{target_path}', engine='xlsxwriter')
    target_df.to_excel(writer, sheet_name='Report', index=False, header=True)
    for i, header in enumerate(constant.timesort_headers):
        target_headers = list(target_df)
        if i <= len(target_headers) - 1:
            if target_headers[i] != header:
                data = ''
                if header in target_headers:
                    data = target_df.pop(header)
                logger('check_columns',
                       f'difference in headers found, excel header: {target_headers}, fixed header: {header}')
                target_df.insert(i, header, data)
                # target_df.loc[5][header] = header
                writer = pd.ExcelWriter(f'{target_path}', engine='xlsxwriter')
                target_df.to_excel(writer, sheet_name='Report', index=False, header=True)
                writer.save()
                writer.close()
    writer.save()
    writer.close()


def undate(x):
    if pd.isnull(x):
        return x
    if re.search(r"^\d{6}-\d{2}-\d{4}$", x):
        return x
    try:
        if '00:00:00' in x:
            return datetime.strptime(x, '%Y-%m-%d %H:%M:%S').date()
        else:
            return x
    except AttributeError:
        return x
    except Exception:
        return x


def format_allowance_master(task_code, inbound_path):
    allowance_path = os.path.join(inbound_path, "Allowance Master.xlsx")
    allowance_df = pd.read_excel(allowance_path,dtype=str)
    allowance_df = allowance_df[allowance_df.columns.intersection(constant.config[task_code]["allowance_master_headers"])]
    allowance_df = allowance_df.reindex(columns=constant.config[task_code]["allowance_master_headers"])
    writer = pd.ExcelWriter(allowance_path, engine='xlsxwriter')
    allowance_df.to_excel(writer, sheet_name='Sheet1', index=False, header=True)
    writer.save()
    writer.close()

# if __name__ == '__main__':
#     ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
#     inbound_path = f'{ROOT_DIR}/inbound'
#     format_allowance_master(inbound_path)
# #     outbound_path = f'{ROOT_DIR}/outbound'
#     target_path = os.path.join(inbound_path, f'QuickPay_Employee_Target.xlsx')
#     target_wb = pd.ExcelFile(target_path)
#     sheets = target_wb.book.worksheets
#     for sheet in sheets:
#         if sheet.sheet_state == 'visible':
#             sheet_title = sheet.title
#             final_df = pd.read_excel(target_path, sheet_name=str(sheet.title), dtype=str, engine='openpyxl')
#             for i in final_df.columns:
#                 final_df[i] = final_df[i].apply(undate)
#             if 'exception' in sheet.title.lower() and final_df.empty:
#                 continue
#             if 'exception' in sheet.title.lower() and pd.isnull(final_df.iloc[0,0]):
#                 continue
#             final_df.replace({pd.NaT: None}, inplace=True)
#             final_df.fillna('', inplace=True)
#             final_df = final_df.applymap(str)
#             final_df = final_df.astype(str)
#             final_df = final_df.replace('NaT', "")
#             writer = pd.ExcelWriter(f'{outbound_path}/{sheet.title}.xlsx', engine='xlsxwriter', date_format = 'yyyy-mm-dd')
#             final_df.to_excel(writer, sheet_name=str(sheet.title), index=False, header=True)
#             writer.save()
#             writer.close()
#
