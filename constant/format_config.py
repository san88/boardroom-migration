DATE_FMT = '%Y-%m-%d'
TIME_FMT = '%H:%M:%S'
DATETIME_FMT = '%Y-%m-%d %H:%M:%S'
DATETIME_REQ_FMT = '%d/%m/%Y %H:%M %p'


def date_to_str_in_fmt(date_obj):
    return date_obj.strftime(DATE_FMT)

def datetime_to_str_in_fmt(datetime_obj):
    return datetime_obj.strftime(DATETIME_FMT)

def datetime_to_str_in_req_fmt(datetime_obj):
    return datetime_obj.strftime(DATETIME_REQ_FMT)

def time_to_str_in_fmt(datetime_obj):
    return datetime_obj.strftime(TIME_FMT)