import os
ROOTDIR = os.path.dirname(os.path.abspath(__file__))


config = {
    "#bdr-qp-ee":{
        "email_subject_regex": "\s*#rpa\s*#bdr-qp-ee\s*for\s*\[[a-zA-Z0-9_-]*\]\s*pgc:\s*[[a-zA-Z0-9_-]*]\s*cal:\s*\[[a-zA-Z0-9_-]*]\s*lgc:\s*[[a-zA-Z0-9_-]*]",
        "no_compulsory_attachment": 7,
        "accept_files": ['source.csv', 'nationality master.xlsx', 'bank master.xlsx','race master.xlsx', 'religion master.xlsx', 'transform template (employee).xlsx', 'epf master.xlsx'],
        "bot_name": "rpa",
        "task_code": "#bdr-qp-ee",
        "constant_path": f"{os.path.join(ROOTDIR, 'QuickPay/Employee')}",
    },
    "#bdr-ts-ee":{
        "email_subject_regex": "\s*#rpa\s*#bdr-ts-ee\s*for\s*\[[a-zA-Z0-9_-]*\]\s*pgc:\s*[[a-zA-Z0-9_-]*]\s*cal:\s*\[[a-zA-Z0-9_-]*]\s*",
        "no_compulsory_attachment": 6,
        "accept_files": ['source.xlsx', 'transform template.xlsx', 'epf master.xlsx','race master.xlsx', 'religion master.xlsx', 'nationality master.xlsx'],
        "bot_name": "rpa",
        "task_code": "#bdr-ts-ee",
        "constant_path": f"{os.path.join(ROOTDIR, 'TimeSoft/Employee')}",
    },
    "#bdr-ts-fa":{
        "email_subject_regex": "\s*#rpa\s*#bdr-ts-fa\s*for\s*\[[a-zA-Z0-9_-]*\]\s*",
        "no_compulsory_attachment": 2,
        "accept_files": ['source.xlsx', 'allowance master.xlsx'],
        "bot_name": "rpa",
        "task_code": "#bdr-ts-fa",
        "constant_path": f"{os.path.join(ROOTDIR, 'TimeSoft/Allowance')}",
        "allowance_master_headers": ['Allowance Code', 'Timesoft Code', 'Allowance Name', 'AllowanceType','Fixed Flag'],

    },

    "#bdr-ts-py": {
        "email_subject_regex": "\s*#rpa\s*#bdr-ts-py\s*for\s*\[[a-zA-Z0-9_-]*\]\s*",
        "no_compulsory_attachment": 2,
        "accept_files": ['source.xlsx', 'allowance master.xlsx'],
        "bot_name": "rpa",
        "task_code": "#bdr-ts-py",
        "constant_path": f"{os.path.join(ROOTDIR, 'TimeSoft/Payroll')}",
        "allowance_master_headers": ['Allowance Code', 'Allowance Name', 'Timesoft Code', 'AllowanceType', 'Fixed Flag'],
    }
}


timesort_headers = ['EMP CODE', 'EMPLOYEE NAME', 'BASIC SALARY', 'ORIGINAL HIRED DATE', 'HIRED DATE', 'RESIGNATION DATE', 'BIRTH DATE',
                    'NRIC', 'OLD IC', 'PASSPORT NO', 'PASSPORT EXP DATE', 'SEX', 'BANK CODE', 'BANK ACC NO', 'PAY TYPE', 'PAY GROUP',
                    'DAYS WK PER WEEK', 'HRS WK PER YEAR (OT)', 'NPL RATE FORMULA', 'DAILY RATE FORMULA (RD)', 'EPF CLASS', 'EPF ACCOUNT NO',
                    'VOLUNTARY EE RATE', 'VOLUNTARY ER RATE', 'SOCSO TYPE', 'INCOME TAX NO', 'INCOME TAX BRH', 'MARITAL STATUS', 'SPOUSE WORKING',
                    'TOTAL ELIGIBLE CHILD POINT', 'DISABLED INDIVIDUAL', 'DISABLED SPOUSE', 'NON RESIDENT TAX FORMULA', 'NATIONALITY CODE',
                    'NATIONALITY DESC', 'RACE CODE', 'RACE DESC', 'RELIGION CODE', 'RELIGION DESC', 'DEPARTMENT CODE', 'DEPARTMENT DESC',
                    'OCCUPATION CODE', 'OCCUPATION DESC', 'COST CTR CODE', 'COST CTR DESC', 'CATEGORY CODE', 'CATEGORY DESC', 'JOB GRADE CODE',
                    'JOB GRADE DESC', 'SECTION CODE', 'SECTION DESC', 'DIVISION CODE', 'DIVISION DESC', 'COMPANY CODE', 'BRANCH CODE', 'BRANCH DESC',
                    'LOCATION CODE', 'LOCATION DESC', 'LEAVE SCHEME CODE', 'LEAVE SCHEME DESC', 'HOLIDAY TABLE', 'SHIFT WORKER', 'BENEFIT SCHEME CODE',
                    'BENEFIT SCHEME DESC', 'ADDRESS1', 'ADDRESS2', 'ADDRESS3', 'POSTAL CODE', 'MOBILE PHONE', 'TELEPHONE', 'COMPANY EMAIL', 'PERSONAL EMAIL',
                    'EMP ALIAS (FOR E-LEAVE)', 'EMERGENCY CONTACT', 'EMERGENCY RELATION', 'EMERGENCY PHONE NO', 'EMERGENCY ADDRESS1', 'EMERGENCY ADDRESS2',
                    'EMERGENCY ADDRESS3', 'EMERGENCY POSTAL', 'SPOUSE NAME', 'EMP PASS #', 'EMP PASS ISSUED DATE', 'EMP PASS EXP DATE', 'TABUNG HAJI NO',
                    'ASB ACCOUNT NO', 'PTPTN NO', 'ZAKAT NO', 'EDUCATION CODE', 'EDUCATION DESC']
