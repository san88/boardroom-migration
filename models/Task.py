import xlwings as xw
import pandas as pd
from rpa_control import getcol
from utils import logger
from utils.create_db import Emails
from utils.insert_values import insert_email_log

def log_email_db(self):
    try:
        email = Emails()
        email.entry_id = self.Message.entry_id
        email.date_time = self.Message.datetime
        email.task_code = self.task_code
        email.sender_email = self.Message.email_address
        email.sender_name = self.Message.sender
        email.email_body = self.Message.email_body
        email.subject = self.Message.email_subject
        insert_email_log(email)
    except Exception as e:
        logger('log_email_db', e, 'ERROR')


class Task:
    def __init__(self, message,task_code):
       self.bot_name ='#rpa'
       self.task_code = task_code
       self.task_status = ""
       self.Message = message

    def log_email_db(self):
        try:
            email = Emails()
            email.entry_id = self.Message.entry_id
            email.date_time = self.Message.datetime
            email.task_code = self.task_code
            email.sender_email = self.Message.email_address
            email.sender_name = self.Message.sender
            # email.sender_location = self.Message.location
            email.sender_location = 'Malaysia'
            email.email_body = self.Message.email_body
            email.subject = self.Message.email_subject
            insert_email_log(email)
        except Exception as e:
            logger('log_email_db', e, 'ERROR')

