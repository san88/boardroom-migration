import pandas as pd
import re
import os
from constant import constant
from rpa_control import unzip, unzip_7z
from utils.select_values import select_email


class Message:
    def __init__(self, input):
        self.entry_id = input['entry_id']
        self.datetime = input['datetime']
        self.sender = input['sender']
        self.email_address = input['email_address']
        self.location = input['location']
        self.email_subject = input['subject']
        self.email_body = input['email_body']
        self.attachments = input['attachments']
        self.zip_file_name = None

    def check_email_entry(self, root_dir):
        df = pd.read_excel(f'{root_dir}/resources/RPA-control.xlsx', sheet_name='email log')
        if self.entry_id in df['entry id'].tolist():
            return False
        else:
            return True

    def check_email_db(self):
        results = select_email(self.entry_id)
        if results:
            return False
        return True

    def check_email_format(self):
        task_code = self.get_task_code().lower()
        subject = self.email_subject
        if re.match(constant.config[task_code]['email_subject_regex'], subject.lower()):
            return True, task_code
        else:
            return False, ""

    def check_email_attachment(self, root_dir):
        task_code = self.get_task_code().lower()
        if (self.attachments.Count > 0):
            for attachment in self.attachments:
                flag = attachment.PropertyAccessor.GetProperty("http://schemas.microsoft.com/mapi/proptag/0x37140003")
                # To ignore embedded attachments
                if flag != 4:
                    if not attachment.FileName.endswith('.zip'):
                        return False
                    else:
                        self.zip_file_name = attachment.FileName
                        attachment.SaveASFile(f'{root_dir}/inbound/{attachment.FileName}')
                        unzip_7z(f'{root_dir}/inbound')
                        if task_code == '#bdr-ts-py':
                            return True
                        matched_file_count = 0
                        for file in os.listdir(f'{root_dir}/inbound'):
                            if file.lower() in constant.config[task_code]['accept_files']:
                                matched_file_count +=1
                        if matched_file_count == constant.config[task_code]['no_compulsory_attachment']:
                            return True
                        else:
                            return False



    def get_task_code(self):
        found = re.findall(r'#bdr-[a-z-A-Z]+-[a-z-A-Z]+', self.email_subject)
        return found[0]
    def get_custom_code(self):
        found = re.findall(r'\[(.*?)\]', str(self.email_subject))
        task_code = self.get_task_code().lower()
        custom_codes = {}
        if 'qp-ee' in task_code:
            keys = ["CompanyCode", "PayGroupCode", "CalendarCode", "LeaveGradeCode"]
        elif 'ts-ee' in task_code:
            keys = ["CompanyCode", "PayGroupCode", "CalendarCode"]
        else:
            keys = ["CompanyCode"]

        for idx, key in enumerate(keys):
            custom_codes.update({key:found[idx]})

        return custom_codes



if __name__ == '__main__':
    x = re.findall(r'\[(.*?)\]', "#rpa #bdr-qp-ee for [31-TrendMicro] pgc:[pgc] cal:[5DAYS]")
    print(x)