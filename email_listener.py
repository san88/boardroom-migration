from apscheduler.schedulers.background import BlockingScheduler
import pythoncom
import win32com.client
import os
import time
from datetime import datetime, timedelta
from constant.constant import config
from constant.format_config import datetime_to_str_in_fmt
from models.Message import Message
from models.Task import Task
from main import quickpay_ee, timesoft_ee, timesoft_payroll, timesoft_allowance
from rpa_control import reply_email, create_path, reply_email_with_attachments_in_path, clear_path
from utils import logger, BotRun, insert_botrun, update_botrun

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
# paths['inbound'] = f'{ROOT_DIR}/inbound'
# paths['outbound'] = f'{ROOT_DIR}/outbound'
# CONSTANT_PATH = f'{ROOT_DIR}/constant'
# TRANSFORM_PATH = f'{ROOT_DIR}/transform'

paths = {
    "inbound": f'{ROOT_DIR}/inbound',
    "outbound":f'{ROOT_DIR}/outbound',
    "transform":f'{ROOT_DIR}/transform',
}
def get_rpa_email(main_inbox):
    main_messages = main_inbox.Items
    rpa_messages = main_inbox.Folders['#rpa'].Items

    lastday = datetime.now() - timedelta(days=1)

    filteredMainmessages = main_messages.Restrict(
        "[ReceivedTime] >= '" + (lastday.strftime(r'%d/%m/%Y %H:%M %p')) + "'")  # standard pm format needed to work
    filteredRpaMessage = rpa_messages.Restrict(
        "[ReceivedTime] >= '" + (lastday.strftime(r'%d/%m/%Y %H:%M %p')) + "'")  # standard pm format needed to work
    print(f'{str(filteredMainmessages.Count)} main inbox emails since {str(lastday)}')
    return filteredRpaMessage


def assign_main(task_code,custom_codes):
    logger('assign_main', f'task_code = {task_code}, custom_codes = {custom_codes}')
    success = False
    paths['constant'] = config[task_code.lower()]['constant_path']
    if task_code.lower() == '#bdr-qp-ee':
        logger('assign_main', 'Starting QuickPay-EE processing')
        success = quickpay_ee("QuickPay_Employee", paths, custom_codes)
    elif task_code.lower() == '#bdr-ts-ee':
        logger('assign_main', 'Starting TimeSoft-EE processing')
        success = timesoft_ee("TimeSoft_Employee", paths, custom_codes)
    elif task_code.lower() == '#bdr-ts-py':
        logger("assign_main", "Starting TimeSoft-PY processing")
        success = timesoft_payroll("TimeSoft_Payroll", task_code, paths, custom_codes)
    elif task_code.lower() == '#bdr-ts-fa':
        logger("assign_main", "Starting TimeSoft-FA processing")
        success = timesoft_allowance("TimeSoft_Allowance", task_code, paths, custom_codes)
    return success


def listen():
    pythoncom.CoInitialize()
    mapi = win32com.client.Dispatch('outlook.application').GetNamespace("MAPI")
    main_inbox = mapi.GetDefaultFolder(6)
    messages = get_rpa_email(main_inbox)
    create_path(paths['inbound'])
    create_path(paths['outbound'])
    create_path(paths['transform'])
    for msg in messages:
            if msg.body.find("#botreply") != 0:
                if '#rpa' in msg.subject and ('#bdr-qp' in msg.subject.lower() or '#bdr-ts' in msg.subject.lower()):
                    start_time = time.perf_counter()
                    test = msg.Sender.GetExchangeUser()
                    email_address = msg.Sender.GetExchangeUser().PrimarySmtpAddress if msg.Sender.GetExchangeUser() else msg.SenderEmailAddress
                    msg_input = {
                        "entry_id": msg.EntryID,
                        "datetime": datetime_to_str_in_fmt(datetime.combine(msg.senton.date(), msg.senton.time())),
                        "sender": msg.sender(),
                        "email_address": email_address.lower(),
                        "location": "",
                        "subject": msg.subject.lower(),
                        "email_body": msg.body,
                        "attachments": msg.Attachments,
                    }
                    message_obj = Message(msg_input)
                    is_matched_format, task_code = message_obj.check_email_format()
                    if is_matched_format:
                        if message_obj.check_email_db():
                            botrun = BotRun()
                            botrun.bot_id = 4
                            botrun.email = email_address
                            botrun.entry_id = msg.EntryID
                            botrun_id = insert_botrun(botrun)
                            try:
                                if message_obj.check_email_attachment(ROOT_DIR):
                                    task_obj = Task(message_obj, task_code)
                                    task_obj.log_email_db()
                                    custom_codes = message_obj.get_custom_code()
                                    success = assign_main(task_obj.task_code,custom_codes)
                                    if success:
                                        end_time = time.perf_counter()
                                        total_execution = end_time - start_time
                                        update_botrun(botrun_id, 1, '')
                                        email_body = f"#botreply: Successfully run BDR bot. You'll find the attachments below."
                                        reply_email_with_attachments_in_path(msg, message_obj.zip_file_name, email_body,paths['outbound'])
                                        clear_path(paths['inbound'])
                                        clear_path(paths['outbound'])
                                        clear_path(paths['transform'])
                                        print("RPA completed")
                                        print(f'Total execution time: {total_execution}')
                                        msg.Move(main_inbox.Folders['#rpa'].Folders[task_obj.task_code])
                                    else:
                                        update_botrun(botrun_id, 1, '')
                                        reply_email(msg, "#botreply: Something is wrong, please contact RPA Team for help.")
                                        msg.Move(main_inbox.Folders['#rpa'].Folders['error-log'])
                                else:
                                    update_botrun(botrun_id, 1, 'Missing necessary attachments')
                                    print("Missing necessary attachments!")
                                    reply_email(msg, "#botreply: Missing necessary attachments! Please resend again.")
                                    msg.Move(main_inbox.Folders['#rpa'].Folders['error-log'])
                            except Exception as e:
                                update_botrun(botrun_id, 2, e)
                                logger('listen', str(e), log_type='ERROR')
                                print(e)
                                print('error')
                                print(
                                    "#botreply: Data Transformation Failed. Please verify email subject or contact RPA Team for help")
                                reply_email(msg,
                                            "#botreply: Data Transformation Failed. Please contact RPA Team for help.")
                                clear_path(paths['inbound'])
                                clear_path(paths['outbound'])
                                msg.Move(main_inbox.Folders['#rpa'].Folders['error-log'])
                    else:
                        print("#botreply: Unknown format. Please verify email subject or contact RPA Team for help")
                        reply_email(msg,"#botreply: Unknown format. Please verify email subject or contact RPA Team for help.")
                        msg.Move(main_inbox.Folders['#rpa'].Folders['error-log'])



if __name__ == '__main__':
    scheduler = BlockingScheduler(standalone=True)
    try:
        scheduler.add_job(listen, 'interval', seconds=5, id='bdr_email_listener', max_instances=1)
        scheduler.start()
    except (KeyboardInterrupt, SystemExit):
        print('KeyboardInterrupt, scheduler shutting down.')
        # scheduler.remove_job(id='email_listener')
        scheduler.shutdown(wait=False)
    except Exception as e:
        logger('main', str(e), 'ERROR')
        print('Exception: ' + str(e))
        # scheduler.remove_job(id='email_listener')
        scheduler.shutdown(wait=False)

