import os
import glob
import re
from bdr_custom_function import process_target_file, export_target_file, to_xlsx, check_columns, rename_transform_template_sheet, \
    format_allowance_master
from export_master import create_raw_source_file, export_master_files
import shutil
from utils import logger
from rpa_control import clear_path

def quickpay_ee(key, paths, custom_codes):
    try:
        logger('quickpay_main', r'Quickpay data migration started...')
        transform_template = os.path.join(paths['inbound'], 'Transform Template (Employee).xlsx')
        csv_files = glob.glob(os.path.join(paths['inbound'], '*.csv'))
        if to_xlsx(csv_files):
            for f in csv_files:
                os.remove(f)

            # copying all files from constant to inbound folder
            for f in os.listdir(paths['constant']):
                shutil.copy(os.path.join(paths['constant'], f), paths['inbound'])

            print("Processing Master Files")
            logger('quickpay_main', 'Creating Raw Source file')
            create_raw_source_file(transform_template)
            logger('quickpay_main', 'Exporting Master Files')
            export_master_files(transform_template, paths['inbound'], paths['outbound'])

            print("Processing Target Files")
            logger('quickpay_main', 'Processing target file')
            process_target_file(key, paths['inbound'], paths['outbound'])
            logger('quickpay_main', 'Exporting target files')
            export_target_file(key, custom_codes, paths['inbound'], paths['outbound'], None)

            return True
        else:
            return False
    except Exception as ex:
        logger('quickpay_main', str(ex), 'ERROR')
        return False


def timesoft_ee(key, paths, custom_codes):
    try:
        print(r'Timesoft data migration started...')
        transform_template = os.path.join(paths['inbound'], 'Transform Template.xlsx')

        # copying all files from constant to inbound folder
        for f in os.listdir(paths['constant']):
            shutil.copy(os.path.join(paths['constant'], f), paths['inbound'])

        # Adding resignation date if the column does not exist in source file
        logger('timesoft_main', 'checking existence of fixed columns')
        check_columns(paths['inbound'])

        print("Processing Master Files")
        logger('timesoft_main', 'Creating Raw Source file')
        create_raw_source_file(transform_template)
        logger('timesoft_main', 'Exporting Master Files')
        export_master_files(transform_template, paths['inbound'], paths['outbound'])

        print("Processing Target Files")
        logger('timesoft_main', 'Processing target file')
        process_target_file(key, paths['inbound'], paths['outbound'])
        logger('timesoft_main', 'Exporting target files')
        export_target_file(key, custom_codes, paths['inbound'], paths['outbound'], None)

        return True
    except Exception as ex:
        logger('timesoft_main', str(ex), 'ERROR')
        return False


def timesoft_allowance(key, task_code, paths, custom_codes):
    logger('timesoft_allowance', 'timesoft allowance data migration started')
    try:
        # copying all files from constant to inbound folder
        for f in os.listdir(paths['constant']):
            shutil.copy(os.path.join(paths['constant'], f), paths['inbound'])

        # rename transform template
        rename_transform_template_sheet(paths['inbound'])

        format_allowance_master(task_code, paths['inbound'])
        # Processing the files required and generating oubound files
        logger('timesoft_allowance', 'Processing target files')
        process_target_file(key, paths['inbound'], paths['outbound'])
        logger('timesoft_allowance', 'exporting target files')
        export_target_file(key, custom_codes, paths['inbound'], paths['outbound'], None)

        return True
    except Exception as ex:
        logger('timesoft_allowance', str(ex), 'ERROR')
        return False


def timesoft_payroll(key, task_code, paths, custom_codes):
    logger('timesoft_payroll', 'timesoft payroll data migration started')
    source_files = [f for f in os.listdir(paths['inbound']) if 'source' in f.casefold()]
    try:
        for index, source_file in enumerate(source_files):
            # copy allowance master to transform
            clear_path(paths['transform'])
            shutil.copy(os.path.join(paths['inbound'], 'Allowance Master.xlsx'), paths['transform'])

            # copying all files from constant to transform folder
            for const_f in os.listdir(paths['constant']):
                shutil.copy(os.path.join(paths['constant'], const_f), paths['transform'])

            # copying source to transform and rename
            shutil.copy(os.path.join(paths['inbound'], source_file),
                        os.path.join(paths['transform'], os.path.join(paths['transform'], 'Source.xlsx')))

            # rename transform template
            custom_sheet_name = rename_transform_template_sheet(paths['transform'])

            format_allowance_master(task_code, paths['transform'])
            # Processing the files required and generating oubound files
            logger('timesoft_payroll', 'Processing target files')
            process_target_file(key, paths['transform'], paths['outbound'])
            logger('timesoft_payroll', 'exporting target files')
            export_target_file(key, custom_codes, paths['transform'], paths['outbound'], custom_sheet_name)
        return True
    except Exception as ex:
        logger('timeosft_payroll', str(ex), 'ERROR')
        return False


# if __name__ == '__main__':
#     ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
#     paths = {
#         "inbound": f'{ROOT_DIR}/inbound',
#         "outbound": f'{ROOT_DIR}/outbound',
#         "constant": f'{ROOT_DIR}/constant/TimeSoft/Payroll',
#         "transform": f'{ROOT_DIR}/transform',
#     }
#     source_files  = [f for f in os.listdir(paths['inbound']) if 'source' in f.casefold() and (re.match('[\d]-[a-zA-Z]+',f))]
#     for idx, source_file in enumerate(source_files):
#         for const_f in os.listdir(paths['constant']):
#             shutil.copy(os.path.join(paths['constant'], const_f), paths['transform'])
#         shutil.copy(os.path.join(paths['inbound'],source_file),
#                     os.path.join(paths['transform'], os.path.join(paths['transform'],'Source.xlsx')))
